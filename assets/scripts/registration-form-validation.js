$(document).ready(function(){
  $("#registration-form").validate({
    rules: {
      firstname: {
        required: true
      },
      lastname: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      organization: {
        required: true
      },
      title: {
        required: false
      },
      country: {
        required: true
      }
    },
    messages: {
      firstname: {
        required: "Please enter your first name."
      },
      lastname: {
        required: "Please enter your last name."
      },
      email: {
        required: "Please enter your email address.",
        email: "Please enter a valid email address."
      },
      organization: {
        required: "Please enter your organization's name."
      },
      country: {
        required: "Please select a country."
      }
    },
    highlight: function(element, errorClass) {
        $(element).addClass("is-invalid");
    },
    unhighlight: function(element, errorClass) {
        $(element).removeClass("is-invalid");
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});